# 概述
> `jasypt-swing-tool`是为了方便平时使用`jasypt-spring-boot`而制作的一个带界面的小工具，可以实现加解密。

在我们的服务中不可避免的需要使用到一些秘钥（数据库、redis等）如果生产如果采用明文配置讲会有安全问题，[jasypt](https://gitee.com/mirrors/Jasypt-Spring-Boot) 
是一个通用的加解密库，我们可以使用它来保护我们的敏感信息。基于方便，我开发了一个易用的界面来使用它。

![加密的配置文件](doc/image/8.png)

# 功能
`jasypt-swing-tool`封装了平时写的加解密工具类，并附带了一个界面，界面自带十几种皮肤可供选择，不怕审美疲劳。

只要双击它就可以运行，再也不用自己写工具类跑了。

# 界面

![jasypt-swing-tool](doc/image/1.png)
![jasypt-swing-tool](doc/image/2.png)
![jasypt-swing-tool](doc/image/3.png)

# 构建

运行主类: `com.shuyangyang.JasyptSwingToolApplication`

## 构建jar包
idea中 Files->Project Structure->Artifacts

![jasypt-swing-tool](doc/image/4.png)

按照上图所示配置。

下一步 Build ->Build Artifacts

![jasypt-swing-tool](doc/image/5.png)

选择构建

![jasypt-swing-tool](doc/image/6.png)

下图中即可找到jar包
![jasypt-swing-tool](doc/image/7.png)

把jar包放到任意地方，需要用时直接双击即可。加密后的字符串放到配置文件中`ENC（'密文)`，超级方便。

# 注意事项

算法选项可以为空，默认为"PBEWITHHMACSHA512ANDAES_256"

```
SimpleStringPBEConfig config = new SimpleStringPBEConfig();
config.setPassword(password);
if (StrUtil.isEmpty(algorithm)) {
    config.setAlgorithm("PBEWITHHMACSHA512ANDAES_256");
} else {
    config.setAlgorithm(algorithm);
}
config.setKeyObtentionIterations("1000");
config.setPoolSize("1");
config.setProviderName("SunJCE");
config.setProviderClassName(null);
config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
config.setIvGeneratorClassName("org.jasypt.iv.RandomIvGenerator");
config.setStringOutputType("base64");
```

以上都可自定义。

# 下载

[jasypt-swing-tool-1.0](https://gitee.com/shuyangyang/jasypt-swing-tool/releases/1.0)