package com.shuyangyang.component;

import cn.hutool.core.util.StrUtil;
import com.shuyangyang.util.ComponentUtil;
import com.shuyangyang.util.EncryptUtil;

import javax.swing.*;
import java.awt.*;

/**
 * Jasypt加解密工具控制台面板
 *
 * @author Yang Yang Shu
 */
public class EncryptControlComponent extends Component {

    private final JFrame jFrame = new JFrame("Jasypt加解密工具");
    private final Container c = jFrame.getContentPane();
    private final JTextArea jTextArea = new JTextArea();
    private final JScrollPane jScrollPane = new JScrollPane(jTextArea);
    private final Font font = new Font("宋体", Font.PLAIN, 16);

    public EncryptControlComponent() {
        //设置窗体的位置及大小
        jFrame.setBounds(600, 200, 910, 700);

        // 设置菜单
        JMenuBar jMenuBar = new JMenuBar();
        JMenu jMenu = new JMenu("皮肤");
        jMenu.setFont(font);

        // 绑定菜单动作
        new ComponentAction().bindMenuAction(jMenu, font);

        jMenuBar.add(jMenu);
        jFrame.setJMenuBar(jMenuBar);

        //布局管理器
        c.setLayout(new BorderLayout());
        //设置按下右上角X号后关闭
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //初始化窗体控件
        init();
        //设置窗体可见
        jFrame.setVisible(true);
    }

    /**
     * 初始化主面板
     */
    void init() {
        // 输入框
        JPanel jPanel = new JPanel();
        jPanel.setLayout(null);

        ComponentUtil.createTextLabel(jPanel, "密  钥：", font, 30, 20, 200, 50);
        JTextField secretKeyTextField = ComponentUtil.createTextField(jPanel, font, 180, 20, 700, 50);

        ComponentUtil.createTextLabel(jPanel, "算  法：", font, 30, 80, 200, 50);
        JTextField algorithmField = ComponentUtil.createTextField(jPanel, font, 180, 80, 700, 50);

        ComponentUtil.createTextLabel(jPanel, "明  文：", font, 30, 140, 200, 50);
        JTextField plainTextField = ComponentUtil.createTextField(jPanel, font, 180, 140, 700, 50);

        ComponentUtil.createTextLabel(jPanel, "密  文：", font, 30, 200, 200, 50);
        JTextField cipherTextField = ComponentUtil.createTextField(jPanel, font, 180, 200, 700, 50);

        // 多行文本区域
        jTextArea.setLineWrap(true);
        jTextArea.setFont(font);

        // 底部按钮
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout());

        JButton encryptionBtn = ComponentUtil.createButton(font, "加密");
        JButton decryptBtn = ComponentUtil.createButton(font, "解密");
        JButton resetBtn = ComponentUtil.createButton(font, "重置");
        doButton(secretKeyTextField, plainTextField, cipherTextField, algorithmField, encryptionBtn, decryptBtn, resetBtn);

        jScrollPane.setBounds(30, 280, 850, 300);
        jPanel.add(jScrollPane);
        c.add(jPanel, "Center");
        buttonPanel.add(encryptionBtn);
        buttonPanel.add(decryptBtn);
        buttonPanel.add(resetBtn);
        c.add(buttonPanel, "South");
    }

    /**
     * 按钮动作事件
     *
     * @param secretKeyTextField 密钥值
     * @param plainTextField     明文值
     * @param cipherTextField    密文值
     * @param algorithmField     算法值
     * @param encryptionBtn      加密按钮
     * @param decryptBtn         解密按钮
     * @param resetBtn           重置按钮
     */
    private void doButton(final JTextField secretKeyTextField,
                          final JTextField plainTextField,
                          final JTextField cipherTextField,
                          final JTextField algorithmField,
                          JButton encryptionBtn,
                          JButton decryptBtn,
                          JButton resetBtn) {
        encryptionBtn.addActionListener(e -> {
            // 输出结果
            String pwd = secretKeyTextField.getText();
            if (StrUtil.isBlank(pwd)) {
                jTextArea.append("请先输入密钥\n");
                return;
            }

            String enValue = plainTextField.getText();
            if (StrUtil.isBlank(enValue)) {
                jTextArea.append("请输入需要加密的字符串\n");
                return;
            }

            // 算法
            String algorithm = algorithmField.getText();
            String result = StrUtil.format("加密前: {} \n加密后: {}\n", enValue, EncryptUtil.enCryptPwd(algorithm, pwd, enValue));
            jTextArea.append(result);
        });

        decryptBtn.addActionListener(e -> {
            String pwd = secretKeyTextField.getText();
            if (StrUtil.isBlank(pwd)) {
                jTextArea.append("请先输入密钥 \n");
                return;
            }

            String deValue = cipherTextField.getText();
            if (StrUtil.isBlank(deValue)) {
                jTextArea.append("请输入需要解密的字符串 \n");
                return;
            }

            // 算法
            String algorithm = algorithmField.getText();
            String result = StrUtil.format("解密前: {} \n解密后: {} \n", deValue, EncryptUtil.deCryptPwd(algorithm, pwd, deValue));
            jTextArea.append(result);
        });

        resetBtn.addActionListener(e -> {
            secretKeyTextField.setText("");
            plainTextField.setText("");
            cipherTextField.setText("");
            algorithmField.setText("");
            jTextArea.setText("");
        });
    }
}
