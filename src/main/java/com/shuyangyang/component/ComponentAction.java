package com.shuyangyang.component;

import com.shuyangyang.constant.EnumThem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 组件动作
 *
 * @author Yang Yang Shu
 */
public class ComponentAction {

    /**
     * 绑定菜单动作
     *
     * @param jMenu 菜单
     * @param font  字体
     */
    public void bindMenuAction(JMenu jMenu, Font font) {
        for (final EnumThem skin : EnumThem.values()) {
            JMenuItem menuItem = new JMenuItem(skin.getThemName());
            menuItem.addActionListener(e -> changeStyle(skin.getThemPath()));
            menuItem.setFont(font);
            jMenu.add(menuItem);
        }
    }

    /**
     * 切换皮肤
     *
     * @param style 风格类
     */
    private void changeStyle(String style) {
        try {
            // 更换界面风格
            UIManager.setLookAndFeel(style);
            // 更新UI界面
            Window[] windows = Window.getWindows();
            for (Window window : windows) {
                if (window.isDisplayable()) {
                    SwingUtilities.updateComponentTreeUI(window);
                }
            }
        } catch (UnsupportedLookAndFeelException | ClassNotFoundException | InstantiationException | IllegalAccessException e1) {
            e1.printStackTrace();
        }
    }
}
