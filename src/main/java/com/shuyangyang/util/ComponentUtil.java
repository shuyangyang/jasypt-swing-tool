package com.shuyangyang.util;

import javax.swing.*;
import java.awt.*;

/**
 * 组件工具类
 *
 * @author Yang Yang Shu
 */
public class ComponentUtil {

    private ComponentUtil() {
    }

    /**
     * 创建输入框前面的标签
     *
     * @param panel       面板
     * @param text        标签文字
     * @param font        字体对象
     * @param labelX      坐标X
     * @param labelY      坐标Y
     * @param labelWidth  标签宽度
     * @param labelHeight 标签高度
     */
    public static void createTextLabel(JPanel panel, String text, Font font, int labelX, int labelY, int labelWidth, int labelHeight) {
        JLabel secretKeyLabel = new JLabel(text);
        secretKeyLabel.setFont(font);
        secretKeyLabel.setBounds(labelX, labelY, labelWidth, labelHeight);
        panel.add(secretKeyLabel);
    }

    /**
     * 创建输入框
     *
     * @param panel       面板
     * @param font        字体对象
     * @param fieldX      坐标X
     * @param fieldY      坐标Y
     * @param fieldWidth  输入框宽度
     * @param fieldHeight 输入框高度
     * @return 文本输入框对象，可以设置其他操作
     */
    public static JTextField createTextField(JPanel panel, Font font, int fieldX, int fieldY, int fieldWidth, int fieldHeight) {
        JTextField secretKeyTextField = new JTextField();
        secretKeyTextField.setFont(font);
        secretKeyTextField.setBounds(fieldX, fieldY, fieldWidth, fieldHeight);
        panel.add(secretKeyTextField);
        return secretKeyTextField;
    }

    /**
     * 创建按钮
     *
     * @param font 字体对象
     * @param text 文本
     * @return 按钮对象
     */
    public static JButton createButton(Font font, String text) {
        JButton button = new JButton(text);
        button.setFont(font);
        return button;
    }
}
