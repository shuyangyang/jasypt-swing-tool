package com.shuyangyang.constant;

/**
 * 可选主题
 *
 * @author Yang Yang Shu
 */
public enum EnumThem {

    /**
     * 柔和黑
     */
    NOIRE_SKIN("com.jtattoo.plaf.noire.NoireLookAndFeel", "柔和黑"),
    /**
     * 木质感风格
     */
    SMART_SKIN("com.jtattoo.plaf.smart.SmartLookAndFeel", "木质感"),
    /**
     * 黄色按钮背景风格
     */
    MINT_SKIN("com.jtattoo.plaf.mint.MintLookAndFeel", "黄色按钮背景"),
    /**
     * 绿色按钮背景风格
     */
    MC_WIN_SKIN("com.jtattoo.plaf.mcwin.McWinLookAndFeel", "绿色按钮背景"),
    /**
     * 纯XP风格
     */
    LUNA_SKIN("com.jtattoo.plaf.luna.LunaLookAndFeel", "纯xp"),
    /**
     * 黑色风格
     */
    HI_FI_SKIN("com.jtattoo.plaf.hifi.HiFiLookAndFeel", "黑色"),
    /**
     * 普通蓝色风格
     */
    FAST_SKIN("com.jtattoo.plaf.fast.FastLookAndFeel", "普蓝"),
    /**
     * 黄色风格
     */
    BERNSTEIN_SKIN("com.jtattoo.plaf.bernstein.BernsteinLookAndFeel", "普黄"),
    /**
     * 翠绿色金属质感
     */
    ALUMINIUM_SKIN("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel", "翠绿色金属"),
    /**
     * xp清新主题
     */
    AERO_SKIN("com.jtattoo.plaf.aero.AeroLookAndFeel", "xp清新"),
    /**
     * 亚克力主题
     */
    ACRYL_SKIN("com.jtattoo.plaf.acryl.AcrylLookAndFeel", "亚克力"),
    /**
     * 石墨风格
     */
    GRAPHITE_SKIN("com.jtattoo.plaf.graphite.GraphiteLookAndFeel", "石墨"),
    /**
     * 质感风格
     */
    TEXTURE_SKIN("com.jtattoo.plaf.texture.TextureLookAndFeel", "浅黑质感");

    /**
     * 主题类路径
     */
    private final String themPath;

    /**
     * 主题名称
     */
    private final String themName;

    EnumThem(String themPath, String themName) {
        this.themPath = themPath;
        this.themName = themName;
    }

    public String getThemPath() {
        return themPath;
    }

    public String getThemName() {
        return themName;
    }
}
