package com.shuyangyang;

import com.shuyangyang.component.EncryptControlComponent;
import com.shuyangyang.constant.EnumThem;

import javax.swing.*;

/**
 * encrypt启动
 *
 * @author Yang Yang Shu
 */
public class JasyptSwingToolApplication {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(EnumThem.MC_WIN_SKIN.getThemPath());
            new EncryptControlComponent();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }

}
